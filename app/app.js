
angular.module('homework', ['ngAnimate', 'toastr']);

var languages = { "DE":"German",
                  "IT":"Italian",
                  "FR":"Franch",
                  "EN":"English"};

var roles = { "UP":"Uploader",
              "AU":"Autor",
              "AD":"ADMIN",
              "SU":"Super Admin",
              "VI":"Vitaly"};


angular.module('homework').controller('Main', function($scope, $http, $timeout, UserService)
{
    console.log("Controller created!");

    var pageSize = 10;

    $scope.users = [];
    $scope.loadMoreEnabled = true;
    $scope.usersSelected = false;

    $scope.currentPage = new Page();

    $scope.filter = {}

    function Page(){
        return {
                start: 0,
                end:   10,
                sort: 'name',
                order: 'asc'
                //error: 'true'
                };
    }

    addUsers($scope.currentPage);

    function addUsers(page)
    {
      //$http.get('/api/users', {params:{start:start, end:end}} )
      UserService.getUsers(page)
        .success(function(result)
        {
            var users = result.data;
            var meta = result.meta;
            console.log('Users loaded', page.start, page.end, 'loaded', users.length, result);
            $scope.users = $scope.users.concat(users);
            $scope.loadMoreEnabled = (users.length == pageSize);
            $scope.usersSelected = false;
        })
        .error(function(result){
        });
    };


    $scope.loadMore = function()
    {
        console.log('LoadMode...');
        var page    = $scope.currentPage;
            page.start  = page.end;
            page.end    += pageSize;
        addUsers(page);
    }

    $scope.sortBy = function(field)
    {
        var newPage = new Page();
        newPage.sort = field;

        if($scope.currentPage.sort === field){  // change order
            newPage.order = ($scope.currentPage.order == 'desc') ? 'asc' : 'desc'
        }

        $scope.currentPage = newPage;
        $scope.users = [];
        addUsers(newPage)
    }

    $scope.applyFilter = function()
    {
        var newPage = new Page()
        newPage.sort = $scope.currentPage.sort,
        newPage.order = $scope.currentPage.order

        $scope.currentPage = newPage;
        $scope.users = [];
        var pageFilter = angular.extend(newPage, $scope.filter);
        console.log("applyFilter()","$scope.filters", $scope.filter);
        console.log("pageFilter", pageFilter);
        addUsers(pageFilter)
    }

// reset filter
    $scope.resetFilter = function()
    {
        $scope.filter = {};
        $scope.users = [];
        var newPage = new Page();
        addUsers(newPage)
    }

    $scope.resetFilterError = function()
    {
        $scope.filter = {};
        $scope.users = [];
        var newPage = new Page();
        newPage.error = true;
        addUsers(newPage);
    }



    // remove user from grid and from DB
    $scope.deleteUser = function(user)
    {
        console.log("deleteUser():", user);

        UserService.deleteUsers(user.id)
          .success(function(result)
          {
            console.log("deleteUser() success");
            $scope.users.splice($scope.users.indexOf(user),1);
          });
    }

    // remove user from grid and from DB
    $scope.deleteSelectedUsers = function()
    {
        var selsectedUsers = $scope.users.filter(function(user) {
          return (user.selected == true);
        });
        var selectedIds = selsectedUsers.map(function(user){
            return user.id;
        });
        console.log("deleteSelectedUsers():",  selectedIds);
        if (selectedIds.length){
            UserService.deleteUsers(selectedIds.join(";"))
              .success(function(result)
              {
                console.log("deleteSelectedUsers() success");
                $scope.users = $scope.users.filter(function(user) {
                  return (!user.selected);
                });
              });
        }
    }

    // on user selected
    $scope.selectionChanged = function()
    {
        var selsectedUsers = $scope.users.filter(function(user) {
          return (user.selected == true);
        });
        $scope.usersSelected = (selsectedUsers.length > 0);
    }

    // Add new user
    $scope.addUser = function()
    {
        console.log("Add new user");
    }
});

// custom filters:

//language filter
angular.module('homework').filter('formatLang', function(){
  return function(text){
    return languages[text];
  }
})

// role filter
angular.module('homework').filter('formatRole', function(){
  return function(text){
    return roles[text];
  }
})

// format date
angular.module('homework').filter('formatDate', function(){
  return function(text){
    return moment(text).fromNow();
  }
})


// ==================================================================
// ============ SERVICES     ========================================
// ==================================================================

// Users service
angular.module('homework').service("UserService", function($http, $timeout, toastr){
    var _this = this;
    _this.busy = false;
    _this.errors = [];

    //GET USERS
    _this.getUsers = function(page)
    {
        _this.busy = true;
        console.log('UserService.getUsers request', page);

        var promise = $http.get('/api/users', {params:page} )
            .success(function(result){
                console.log('UserService.getUsers result', result.data.length);
                toastr.info('Users loaded:' + result.data.length);
                _this.busy = false;
            })
            .error(function(data, status){
                var errorMsg = (data && data.error) ? data.error : "Request failed";
                console.error('UserService.getUsers:: ' + errorMsg);
                //toastr.error(errorMsg , 'Error');
                $timeout(function() { removeError(errorMsg)},5000);
                _this.errors.push(errorMsg) ;
                _this.busy = false;
            });
        return promise;
    };


    function removeError(errorMsg){
        _this.errors.splice(_this.errors.indexOf(errorMsg), 1);
    };

    //DELETE USERS
    _this.deleteUsers = function(ids)
    {
        _this.busy = true;
        console.log('UserService.deleteUsers request', ids);

        var promise = $http.delete('/api/users/'+ids)
            .success(function(result){
                console.log('UserService.deleteUsers result', result);
                _this.busy = false;
            })
            .error(function(data, status){
                var errorMsg = (data && data.error) ? data.error : "Request failed";
                console.error('UserService.deleteUsers:: ' + errorMsg);
                //toastr.error(errorMsg , 'Error');

                _this.busy = false;
            });
        return promise;
    };
})


// ==================================================================
// ============ DIRECTIVES   ========================================
// ==================================================================

// Simple directive to show / hide the spinner while loader is busy
angular.module('homework').directive('xtLoader', function(UserService){
	return {
		restrict: 'E',
		replace: true,
		scope: {},
		templateUrl: function($element, $attrs){
			return $attrs.template;
		},
		link: function($scope, $element, $attrs){
			$scope.loader = UserService;
		},
	};
});

// Simple directive to show / hide the spinner while loader is busy
angular.module('homework').directive('xtError', function(UserService){
	return {
		restrict: 'E',
		replace: true,
		scope: {
        },
		templateUrl: function($element, $attrs){
			return $attrs.template;
		},
		link: function($scope, $element, $attrs){
			$scope.service = UserService;
		},
	};
});
